import 'dart:async';
import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

const String WEIGHT_DATABASE = "Weight";
const String HEIGHT_DATABASE = "Height";
const int DEFAULT_HEIGHT_VALUE = 180;

class DatabaseProvider {
  static final DatabaseProvider dbProvider = DatabaseProvider();
  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await createDatabase();
    return _database;
  }

  createDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "flutterBmi.db");
    var database = await openDatabase(path,
        version: 1, onCreate: initDB, onUpgrade: onUpgrade);
    return database;
  }

  void onUpgrade(Database database, int oldVersion, int newVersion) {}

  void initDB(Database database, int version) async {
    String createTableWeight = "CREATE TABLE $WEIGHT_DATABASE ("
        "id INTEGER PRIMARY KEY, "
        "value REAL, "
        "date TEXT "
        ")";
    String createTableHeight = "CREATE TABLE $HEIGHT_DATABASE ("
        "id INTEGER PRIMARY KEY, "
        "value INTEGER"
        ")";
    String addDefaultValueToHeight =
        "INSERT INTO $HEIGHT_DATABASE(value) VALUES($DEFAULT_HEIGHT_VALUE)";

    await database.execute(createTableWeight);
    await database.execute(createTableHeight);
    await database.execute(addDefaultValueToHeight);
  }
}
