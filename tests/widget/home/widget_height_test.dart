import 'package:flutter/material.dart';
import 'package:flutter_demo_health_meetup/model/Height.dart';
import 'package:flutter_demo_health_meetup/widgets/home/HeightWidget.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../mock/block.dart';

void main() {
  testWidgets('Can display widget with a passed default value',
      (WidgetTester tester) async {
        final mockBloc = MockBlocHeight();

    when(mockBloc.heights).thenAnswer(
        (_) => Stream<List<Height>>.value([Height(id: 1, value: 181)]));

    await tester
        .pumpWidget(MaterialApp(home: Scaffold(body: HeightWidget(mockBloc))));
    await tester.pump();

    var slider = getSlider();

    expect(slider.value, equals(181.0));
  });

  testWidgets('Can display animation while data is loading',
      (WidgetTester tester) async {
    final mockBloc = MockBlocHeight();

    when(mockBloc.heights).thenAnswer((_) => Stream<List<Height>>.value(
        null)); //this will give in output the stream that the StreamBuilder is listening to (_bloc.load)

    await tester
        .pumpWidget(MaterialApp(home: Scaffold(body: HeightWidget(mockBloc))));
    await tester.pump();

    var text = getText();

    expect(text.data, "Incoming data...");
  });

  testWidgets('Can display message is no data', (WidgetTester tester) async {
    final mockBloc = MockBlocHeight();

    when(mockBloc.heights).thenAnswer((_) => Stream<List<Height>>.value(
        [])); //this will give in output the stream that the StreamBuilder is listening to (_bloc.load)

    await tester
        .pumpWidget(MaterialApp(home: Scaffold(body: HeightWidget(mockBloc))));
    await tester.pump();

    var text = getText();

    expect(text.data, "No data");
  });

  testWidgets('Can display selection', (WidgetTester tester) async {
    final mockBloc = MockBlocHeight();

    when(mockBloc.heights).thenAnswer((_) => Stream<List<Height>>.value([
          Height(id: 1, value: 181)
        ])); //this will give in output the stream that the StreamBuilder is listening to (_bloc.load)

    await tester
        .pumpWidget(MaterialApp(home: Scaffold(body: HeightWidget(mockBloc))));
    await tester.pump();

    var textWidget = getText();

    expect(textWidget.data, equals("HEIGHT: 181cm"));
  });

  testWidgets('Can save selection', (WidgetTester tester) async {
    final mockBloc = MockBlocHeight();

    when(mockBloc.heights).thenAnswer((_) => Stream<List<Height>>.value([
          Height(id: 1, value: 181)
        ])); //this will give in output the stream that the StreamBuilder is listening to (_bloc.load)

    await tester
        .pumpWidget(MaterialApp(home: Scaffold(body: HeightWidget(mockBloc))));
    await tester.pump();

    var sliderWidget = getSlider();

    sliderWidget.onChangeEnd(179.0);

    var capturedArgument =
        verify(mockBloc.updateHeight(captureAny)).captured.single as Height;
    expect(capturedArgument.id, equals(1));
    expect(capturedArgument.value, equals(179));
  });

  testWidgets('Can display acutal selection without saving',
      (WidgetTester tester) async {
    final mockBloc = MockBlocHeight();

    when(mockBloc.heights).thenAnswer((_) => Stream<List<Height>>.value([
          Height(id: 1, value: 181)
        ])); //this will give in output the stream that the StreamBuilder is listening to (_bloc.load)

    await tester
        .pumpWidget(MaterialApp(home: Scaffold(body: HeightWidget(mockBloc))));
    await tester.pump();

    var sliderWidget = getSlider();

    sliderWidget.onChanged(179.0);
    await tester.pump();

    expect(getText().data, equals("HEIGHT: 179cm"));
    expect(getSlider().value, equals(179.0));

    verifyNever(mockBloc.updateHeight(any));
  });
}

Text getText() => find.byType(Text).first.evaluate().single.widget as Text;

Slider getSlider() =>
    find.byType(Slider).first.evaluate().single.widget as Slider;
