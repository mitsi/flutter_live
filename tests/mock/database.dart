import 'package:flutter_demo_health_meetup/database/Database.dart';
import 'package:mockito/mockito.dart';
import 'package:sqflite/sqlite_api.dart';

class MockDatabaseFactory extends Mock implements DatabaseFactory {}

class MockDatabaseExecutor extends Mock implements Database {}

class MockDatabaseProvider extends Mock implements DatabaseProvider {}
