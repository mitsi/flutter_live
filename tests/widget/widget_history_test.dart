import 'package:flutter/material.dart';
import 'package:flutter_demo_health_meetup/model/Weight.dart';
import 'package:flutter_demo_health_meetup/widgets/History.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../mock/block.dart';

void main() {
  testWidgets(
      'Basic Displaymessage on empty listview and floating button, modal hidden by default',
      (WidgetTester tester) async {
    final mockBloc = MockBlocWeight();
    when(mockBloc.weights).thenAnswer((_) => Stream<List<Weight>>.value([]));

    await tester.pumpWidget(MaterialApp(
      home: History(
        weightBloc: mockBloc,
      ),
    ));
    await tester.pump();

    var waitingText = getText();
    var floatingButton = find.byType(FloatingActionButton);
    var listtWidget = find.byType(ListView);

    expect(waitingText.data, equals("No entries...."));
    expect(listtWidget, findsNothing);
    expect(floatingButton, findsOneWidget);
    expect(find.byType(SingleChildScrollView), findsNothing);
  });

  testWidgets('if there is one data, display list',
      (WidgetTester tester) async {
    final mockBloc = MockBlocWeight();
    when(mockBloc.weights).thenAnswer((_) => Stream<List<Weight>>.value([
          Weight(
              id: 1, value: 80.0, date: DateTime(2020, 03, 02, 0, 0, 0, 0, 0))
        ]));

    await tester.pumpWidget(MaterialApp(
      home: History(
        weightBloc: mockBloc,
      ),
    ));
    await tester.pump();

    expect(find.byType(ListView), findsOneWidget);
  });

  testWidgets('a tile has a title with weight and a subtitle with data',
      (WidgetTester tester) async {
    final mockBloc = MockBlocWeight();
    when(mockBloc.weights).thenAnswer((_) => Stream<List<Weight>>.value([
          Weight(
              id: 1, value: 80.0, date: DateTime(2020, 03, 02, 0, 0, 0, 0, 0))
        ]));

    await tester.pumpWidget(MaterialApp(
      home: History(
        weightBloc: mockBloc,
      ),
    ));
    await tester.pump();

    var listTile = find
        .descendant(
            of: find.byType(Dismissible), matching: find.byType(ListTile))
        .first
        .evaluate()
        .single
        .widget as ListTile;
    expect((listTile.title as Text).data, equals("80.0"));
    expect((listTile.subtitle as Text).data, equals("2020-03-02 00:00:00.000"));
  });

  testWidgets('Dissmisble tile only right to left grey back background',
          (WidgetTester tester) async {
        final mockBloc = MockBlocWeight();
        when(mockBloc.weights).thenAnswer((_) =>
        Stream<List<Weight>>.value([
          Weight(
              id: 1,
              value: 80.0,
              date: DateTime(
                  2020,
                  03,
                  02,
                  0,
                  0,
                  0,
                  0,
                  0))
        ]));

        await tester.pumpWidget(MaterialApp(
          home: History(
            weightBloc: mockBloc,
          ),
        ));
        await tester.pump();

        expect(
            await findDismissibleWidget()
                .confirmDismiss(DismissDirection.endToStart),
            isTrue);
        expect(
            await findDismissibleWidget()
                .confirmDismiss(DismissDirection.startToEnd),
            isFalse);
        expect(
            ((findDismissibleWidget().background as Container).decoration
            as BoxDecoration)
                .color,
            Colors.black54);
      });

  testWidgets('Floating button has an icon', (WidgetTester tester) async {
    final mockBloc = MockBlocWeight();
    when(mockBloc.weights).thenAnswer((_) => Stream<List<Weight>>.value([]));

    await tester.pumpWidget(MaterialApp(
      home: History(
        weightBloc: mockBloc,
      ),
    ));
    await tester.pump();

    var icon = find
        .descendant(
            of: find.byType(FloatingActionButton), matching: find.byType(Icon))
        .first
        .evaluate()
        .single
        .widget as Icon;
    expect(icon.icon, equals(Icons.add));
  });

  testWidgets('Tap on floating button display a modal',
      (WidgetTester tester) async {
    final mockBloc = MockBlocWeight();
    when(mockBloc.weights).thenAnswer((_) => Stream<List<Weight>>.value([]));

    await tester.pumpWidget(MaterialApp(
      home: History(
        weightBloc: mockBloc,
      ),
    ));
    await tester.pump();

    await openModal(tester);

    expect(find.byType(SingleChildScrollView), findsOneWidget);

    await closeOverlay(tester);
  });

  testWidgets('Modal contains a title, an input textfield and a button',
      (WidgetTester tester) async {
    final mockBloc = MockBlocWeight();
    when(mockBloc.weights).thenAnswer((_) => Stream<List<Weight>>.value([]));

    await tester.pumpWidget(MaterialApp(
      home: History(
        weightBloc: mockBloc,
      ),
    ));
    await tester.pump();

    await openModal(tester);

    expect(
        find.descendant(
          of: find.byType(SingleChildScrollView),
          matching: find.byType(TextFormField),
        ),
        findsOneWidget);

    expect(
        find.descendant(
          of: find.byType(SingleChildScrollView),
          matching: find.byType(IconButton),
        ),
        findsOneWidget);

    await closeOverlay(tester);
  });

  testWidgets('Can add a value from overlay', (WidgetTester tester) async {
    final mockBloc = MockBlocWeight();
    when(mockBloc.weights).thenAnswer((_) => Stream<List<Weight>>.value([]));

    await tester.pumpWidget(MaterialApp(
      home: History(
        weightBloc: mockBloc,
      ),
    ));
    await tester.pump();

    await openModal(tester);

    await tester.enterText(find.byType(TextFormField), "90.0");
    await tester.pump();
    var icon =
        find.byType(IconButton).first.evaluate().single.widget as IconButton;
    icon.onPressed();

    var addedValue =
        verify(mockBloc.addWeight(captureAny)).captured.single as Weight;
    expect(addedValue.value, equals(90.0));
    await closeOverlay(tester);
  });

  test("only add double value", () async {
    expect(formatNumber("90"), equals(90.0));
    expect(formatNumber("090"), equals(90.0));
    expect(formatNumber("90,0"), equals(90.0));
    expect(formatNumber("09,0"), equals(9.0));
    expect(formatNumber("09.0"), equals(9.0));
    expect(formatNumber("90.0"), equals(90.0));
    expect(formatNumber("90.5"), equals(90.5));
    expect(formatNumber("9000.000"), equals(9000.0));
    expect(formatNumber("9000 000"), equals(null));
  });
}

ListView getListViewWidget() =>
    find.byType(ListView).first.evaluate().single.widget as ListView;

Dismissible findDismissibleWidget() =>
    find.byType(Dismissible).first.evaluate().single.widget as Dismissible;

Future closeOverlay(WidgetTester tester) async {
  await tester.tap(find.byType(Text).first);
  await tester.pump();
}

Future openModal(WidgetTester tester) async {
  await tester.tap(find.byType(FloatingActionButton).first);
  await tester.pump();
}

Text getText() => find.byType(Text).first.evaluate().single.widget as Text;
