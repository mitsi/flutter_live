import 'package:flutter/material.dart';

class Gender extends StatefulWidget {
  @override
  GenderState createState() => GenderState();
}

class GenderState extends State<Gender> {
  GenderType selected = GenderType.MALE;

  @override
  Widget build(BuildContext context) => Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _buildGenderChoice(context, GenderType.FEMALE, true),
            _buildGenderChoice(context, GenderType.MALE, false),
          ],
        ),
      );

  Widget _buildGenderChoice(
          BuildContext context, GenderType gender, bool leftMargin) =>
      Expanded(
        child: GestureDetector(
          onTap: () => setState(() => {selected = gender}),
          child: Container(
            padding: EdgeInsets.all(10.0),
            margin: leftMargin ? EdgeInsets.only(right: 5) : EdgeInsets.only(left: 5),
            decoration: BoxDecoration(
                border: Border.all(
                    width: 5.0,
                    color: gender == selected
                        ? Colors.yellow
                        : Colors.blueGrey),
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.transparent),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  'resources/assets/${getEnumValue(gender.toString().toLowerCase())}.png',
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(getEnumValue(gender.toString().toLowerCase()), style: TextStyle(fontSize: 20.0))
              ],
            ),
          ),
        ),
      );
}

enum GenderType { MALE, FEMALE }
String getEnumValue(String value) => value.toString().split('.').last;