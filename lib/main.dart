import 'dart:ui' show Color;

import 'package:flutter/material.dart';
import 'package:flutter_demo_health_meetup/widgets/BottomNavigationBar.dart';

void main() => runApp(new MeetupApp());

class MeetupApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'MeetUp Flutter App Mitsi',
        theme: ThemeData(
          primarySwatch: AppMaterialColor,
        ),
      home: CustomBottomNavigationBar()
    );
  }
}
const AppMaterialColor = const MaterialColor(0xFFAA2526, const<int, Color>{
  50: const Color(0xFFAA2526),
  100: const Color(0xFFAA2526),
  200: const Color(0xFFAA2526),
  300: const Color(0xFFAA2526),
  400: const Color(0xFFAA2526),
  500: const Color(0xFFAA2526),
  600: const Color(0xFFAA2526),
  700: const Color(0xFFAA2526),
  800: const Color(0xFFAA2526),
  900: const Color(0xFFAA2526),
});