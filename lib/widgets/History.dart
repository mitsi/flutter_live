import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_demo_health_meetup/bloc/WeightBloc.dart';
import 'package:flutter_demo_health_meetup/model/Weight.dart';

class History extends StatefulWidget {
  History({Key key, this.weightBloc}) : super(key: key);
  final WeightBloc weightBloc;

  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Weight history"),
      ),
      body: Center(
        child: buildStream(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          _showAddTodoSheet(context);
        },
      ),
    );
  }

  buildStream() => StreamBuilder(
        stream: widget.weightBloc.weights,
        builder: (BuildContext context, AsyncSnapshot<List<Weight>> snapshot) {
          return buildListView(snapshot);
        },
      );

  Widget buildListView(AsyncSnapshot<List<Weight>> snapshot) {
    return snapshot.data != null && snapshot.data.length > 0
        ? ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) => Dismissible(
              key: Key("${snapshot.data[index].id}"),
              confirmDismiss: (direction) async =>
                  direction == DismissDirection.endToStart,
              onDismissed: (direction) {
                widget.weightBloc.deleteWeightById(snapshot.data[index].id);
                Scaffold.of(context).hideCurrentSnackBar();
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text("${snapshot.data[index].date} removed"),
                ));
              },
              background: buildBackground(),
              child: buildListTile(snapshot.data[index]),
            ),
          )
        : Text("No entries....");
  }

  ListTile buildListTile(Weight weight) {
    return ListTile(
      title: Text("${weight.value}"),
      subtitle: Text("${weight.date}"),
    );
  }

  Container buildBackground() {
    return Container(
      color: Colors.black54,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Icon(Icons.delete_forever),
          )
        ],
      ),
    );
  }

  void _showAddTodoSheet(BuildContext context) {
    final _weightValueFormController = TextEditingController();

    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return SingleChildScrollView(
              child: Container(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: (BuildContext context) {
              return Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(10.0),
                        topRight: const Radius.circular(10.0))),
                child: Padding(
                  padding: EdgeInsets.only(
                      left: 15, top: 25.0, right: 15, bottom: 30),
                  child: generateRow(_weightValueFormController, context),
                ),
              );
            }(context),
          ));
        });
  }

  Row generateRow(
      TextEditingController _weightValueFormController, BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: TextFormField(
            keyboardType: TextInputType.number,
            controller: _weightValueFormController,
            maxLines: 1,
            style: TextStyle(fontSize: 21, fontWeight: FontWeight.w400),
            autofocus: true,
            decoration: const InputDecoration(
                labelText: 'My weight',
                hintText: '80.3',
                labelStyle:
                    TextStyle(color: Colors.blue, fontWeight: FontWeight.w500)),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 5, top: 15),
          child: CircleAvatar(
            backgroundColor: Colors.blue,
            radius: 18,
            child: IconButton(
              icon: Icon(
                Icons.send,
                size: 22,
                color: Colors.white,
              ),
              onPressed: () {
                var weightEntry =
                    formatNumber(_weightValueFormController.value.text);
                if (weightEntry == null) return;
                final weight = Weight(value: weightEntry, date: DateTime.now());
                if (weight.value > 0) {
                  widget.weightBloc.addWeight(weight);
                  Navigator.pop(context);
                }
              },
            ),
          ),
        )
      ],
    );
  }
}

double formatNumber(String value) {
  var regExp = RegExp(r"^\d*[.]?\d*$");
  var checkNumber = regExp
      .allMatches(value.replaceAll(",", "."))
      .map((m) => m.group(0))
      .toList();
  return checkNumber.length > 0 ? double.parse(checkNumber[0]) : null;
}
