import 'package:flutter/material.dart';
import 'package:flutter_demo_health_meetup/bloc/HeightBloc.dart';
import 'package:flutter_demo_health_meetup/bloc/WeightBloc.dart';
import 'package:flutter_demo_health_meetup/dao/HeightDao.dart';
import 'package:flutter_demo_health_meetup/dao/WeightDao.dart';
import 'package:flutter_demo_health_meetup/database/Database.dart';
import 'package:flutter_demo_health_meetup/repository/HeightRepository.dart';
import 'package:flutter_demo_health_meetup/repository/WeightRepository.dart';

import 'Home.dart';
import 'History.dart';
import 'Info.dart';

class CustomBottomNavigationBar extends StatefulWidget {
  CustomBottomNavigationBar({Key key}) : super(key: key);

  @override
  _CustomBottomNavigationBarState createState() =>
      _CustomBottomNavigationBarState();
}

class NavigationItem {
  const NavigationItem(this.title, this.icon, this.color);

  final String title;
  final IconData icon;
  final MaterialColor color;
}

const List<NavigationItem> allDestinations = <NavigationItem>[
  NavigationItem('Home', Icons.home, Colors.teal),
  NavigationItem('Info', Icons.info_outline, Colors.cyan),
  NavigationItem('Weights', Icons.format_list_bulleted, Colors.orange),
];

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar>
    with TickerProviderStateMixin<CustomBottomNavigationBar> {
  int _selectedIndex = 0;

  static final dbProvider = DatabaseProvider();
  static WeightBloc weightBloc = WeightBloc(WeightRepository(WeightDao(dbProvider)));
  static HeightBloc heightBloc = HeightBloc(HeightRepository(HeightDao(dbProvider)));

  List<Widget> _widgetOptions = <Widget>[
    Home(weightBloc: weightBloc, heightBloc: heightBloc,),
    Info(),
    History(weightBloc: weightBloc,),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: false,
        child: IndexedStack(
          index: _selectedIndex,
          children: _widgetOptions,
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: allDestinations.map((NavigationItem navItem) {
          return BottomNavigationBarItem(
              icon: Icon(navItem.icon),
              backgroundColor: navItem.color,
              title: Text(navItem.title));
        }).toList(),
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
