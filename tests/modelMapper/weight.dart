import 'package:flutter_demo_health_meetup/model/Weight.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('can map object', () async {
    var result = Weight.fromDatabaseJson({
      "id": 1,
      "value": 80.0,
      "date": "2020-03-03T00:00:00.000"
    });

    expect(result.id, equals(1));
    expect(result.value, equals(80.0));
    expect(result.date.toIso8601String(), equals("2020-03-03T00:00:00.000"));
  });

  test('can map object', () async {
    var result = Weight(id: 1, value: 80.0, date: DateTime(2020,03,03,00,00,00)).toDatabaseJson();

    expect(result["id"], equals(1));
    expect(result["value"], equals(80.0));
    expect(result["date"], equals("2020-03-03T00:00:00.000"));
  });
}
