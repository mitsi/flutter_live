import 'package:flutter/material.dart';
import 'package:flutter_demo_health_meetup/bloc/HeightBloc.dart';
import 'package:flutter_demo_health_meetup/bloc/WeightBloc.dart';
import 'package:flutter_demo_health_meetup/widgets/home/WeightChart.dart';

import 'home/Age.dart';
import 'home/Gender.dart';
import 'home/HeightWidget.dart';

class Home extends StatefulWidget {
  Home({Key key, this.weightBloc, this.heightBloc}) : super(key: key);
  final WeightBloc weightBloc;
  final HeightBloc heightBloc;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 6,
                    child: Gender(),
                  ),
                  Expanded(
                    flex: 4,
                    child: Age(),
                  ),
                ],
              ),
            ),
            HeightWidget(widget.heightBloc),
            WeightChart(
              weightBloc: widget.weightBloc,
              heightBloc: widget.heightBloc,
            ),
          ],
        ),
      ),
    );
  }
}
