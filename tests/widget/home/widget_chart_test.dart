import 'package:charts_flutter/flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo_health_meetup/model/Height.dart';
import 'package:flutter_demo_health_meetup/model/Weight.dart';
import 'package:flutter_demo_health_meetup/widgets/home/WeightChart.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../mock/block.dart';

void main() {
  final height = Height(id: 1, value: 180);
  final weight =
      Weight(id: 1, value: 80, date: DateTime(2020, 03, 03, 00, 00, 00));

  testWidgets('Display message if weight data is empty',
      (WidgetTester tester) async {
    MockBlocHeight heightBloc = MockBlocHeight();
    when(heightBloc.heights)
        .thenAnswer((_) => Stream<List<Height>>.value([height]));
    MockBlocWeight weightBloc = MockBlocWeight();
    when(weightBloc.weights).thenAnswer((_) => Stream<List<Weight>>.value([]));

    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
            body: WeightChart(
      heightBloc: heightBloc,
      weightBloc: weightBloc,
    ))));
    await tester.pump();
    await tester.pump();

    expect(find.text("No history entry..."), findsOneWidget);
  });

  testWidgets('Display message if weight data is null',
      (WidgetTester tester) async {
    MockBlocHeight heightBloc = MockBlocHeight();
    when(heightBloc.heights)
        .thenAnswer((_) => Stream<List<Height>>.value([height]));
    MockBlocWeight weightBloc = MockBlocWeight();
    when(weightBloc.weights)
        .thenAnswer((_) => Stream<List<Weight>>.value(null));

    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
            body: WeightChart(
      heightBloc: heightBloc,
      weightBloc: weightBloc,
    ))));
    await tester.pump();

    expect(find.text("History is null o.o"), findsOneWidget);
  });

  testWidgets('Display message if height data is empty',
      (WidgetTester tester) async {
    MockBlocHeight heightBloc = MockBlocHeight();
    when(heightBloc.heights).thenAnswer((_) => Stream<List<Height>>.value([]));
    MockBlocWeight weightBloc = MockBlocWeight();
    when(weightBloc.weights)
        .thenAnswer((_) => Stream<List<Weight>>.value([weight]));

    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
            body: WeightChart(
      heightBloc: heightBloc,
      weightBloc: weightBloc,
    ))));
    await tester.pump();

    expect(find.text("No height entry..."), findsOneWidget);
  });

  testWidgets('Display message if height data is null',
      (WidgetTester tester) async {
    MockBlocHeight heightBloc = MockBlocHeight();
    when(heightBloc.heights)
        .thenAnswer((_) => Stream<List<Height>>.value(null));
    MockBlocWeight weightBloc = MockBlocWeight();
    when(weightBloc.weights)
        .thenAnswer((_) => Stream<List<Weight>>.value([weight]));

    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
            body: WeightChart(
      heightBloc: heightBloc,
      weightBloc: weightBloc,
    ))));
    await tester.pump();

    expect(find.text("Height is null o.o"), findsOneWidget);
  });

  testWidgets('Display chart', (WidgetTester tester) async {
    MockBlocHeight heightBloc = MockBlocHeight();
    when(heightBloc.heights)
        .thenAnswer((_) => Stream<List<Height>>.value([height]));
    MockBlocWeight weightBloc = MockBlocWeight();
    when(weightBloc.weights)
        .thenAnswer((_) => Stream<List<Weight>>.value([weight]));

    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
            body: Column(
              children: <Widget>[
                WeightChart(
                  heightBloc: heightBloc,
                  weightBloc: weightBloc,
                ),
              ],
            ))));
    await tester.pump();

    expect(find.byType(TimeSeriesChart), findsOneWidget);
  });

  testWidgets('By default selection is empty', (WidgetTester tester) async {
    MockBlocHeight heightBloc = MockBlocHeight();
    when(heightBloc.heights)
        .thenAnswer((_) => Stream<List<Height>>.value([height]));
    MockBlocWeight weightBloc = MockBlocWeight();
    when(weightBloc.weights)
        .thenAnswer((_) => Stream<List<Weight>>.value([weight]));

    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
            body: Column(
              children: <Widget>[
                WeightChart(
                  heightBloc: heightBloc,
                  weightBloc: weightBloc,
                ),
              ],
            ))));
    await tester.pump();

    expect(find.text("No selection"), findsOneWidget);
  });

  testWidgets('Display selection', (WidgetTester tester) async {
    MockBlocHeight heightBloc = MockBlocHeight();
    when(heightBloc.heights)
        .thenAnswer((_) => Stream<List<Height>>.value([height]));
    MockBlocWeight weightBloc = MockBlocWeight();
    when(weightBloc.weights)
        .thenAnswer((_) => Stream<List<Weight>>.value([weight]));

    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
            body: Column(
              children: <Widget>[
                WeightChart(
                  heightBloc: heightBloc,
                  weightBloc: weightBloc,
                ),
              ],
            ))));
    await tester.pump();
    await tester.tap(find.byType(TimeSeriesChart));
    await tester.pump();

    expect(find.text("Selected: Mar 3, 2020 80.0kg, BMI:24.69"), findsOneWidget);
  });
}
