import 'package:flutter/material.dart';
import 'package:flutter_demo_health_meetup/widgets/BottomNavigationBar.dart';
import 'package:flutter_demo_health_meetup/widgets/History.dart';
import 'package:flutter_demo_health_meetup/widgets/Home.dart';
import 'package:flutter_demo_health_meetup/widgets/Info.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Home page safeare, 3 icons amber on selected one and can navigate',
      (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: CustomBottomNavigationBar(),
    ));
    expect(find.byType(SafeArea), findsNWidgets(4));
    expect(find.byType(BottomNavigationBar), findsOneWidget);

    var bottomNavigationBar = findBottomNavigationBar();
    expect(bottomNavigationBar.items.length, 3);
    expect(icon(bottomNavigationBar, 0).icon, Icons.home);
    expect(title(bottomNavigationBar, 0).data, "Home");
    expect(icon(bottomNavigationBar, 1).icon, Icons.info_outline);
    expect(title(bottomNavigationBar, 1).data, "Info");
    expect(icon(bottomNavigationBar, 2).icon, Icons.format_list_bulleted);
    expect(title(bottomNavigationBar, 2).data, "Weights");

    expect(findIndexedStack().children[0] is Home, true);
    expect(findIndexedStack().children[1] is Info, true);
    expect(findIndexedStack().children[2] is History, true);


    expect(bottomNavigationBar.selectedItemColor, Colors.amber[800]);

    expect(findBottomNavigationBar().currentIndex, 0);
    expect(findIndexedStack().index, 0);

    await tester.tap(find.byWidget(bottomNavigationBar.items[1].icon));
    await tester.pump();

    expect(findBottomNavigationBar().currentIndex, 1);
    var indexedStackAfterTap = findIndexedStack();
    expect(indexedStackAfterTap.index, 1);
    expect(indexedStackAfterTap.children[indexedStackAfterTap.index] is Info, true);
  });
}

BottomNavigationBar findBottomNavigationBar() =>
    find.byType(BottomNavigationBar).first.evaluate().single.widget
        as BottomNavigationBar;

IndexedStack findIndexedStack() =>
    find.byType(IndexedStack).first.evaluate().single.widget as IndexedStack;

Text title(BottomNavigationBar bottomNavigationBar, int index) =>
    (bottomNavigationBar.items[index].title as Text);

Icon icon(BottomNavigationBar bottomNavigationBar, int index) =>
    (bottomNavigationBar.items[index].icon as Icon);
