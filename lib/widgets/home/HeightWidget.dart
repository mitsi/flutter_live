import 'package:flutter/material.dart';
import 'package:flutter_demo_health_meetup/bloc/HeightBloc.dart';
import 'package:flutter_demo_health_meetup/model/Height.dart';

class HeightWidget extends StatefulWidget {
  final HeightBloc heightBloc;

  HeightWidget(this.heightBloc) : super();

  @override
  _HeightState createState() => _HeightState();
}

class _HeightState extends State<HeightWidget> {
  double minHeight = 100;
  int _height = 100;
  double maxHeight = 250;
  bool isFirstRender = true;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: widget.heightBloc.heights,
        builder: (BuildContext context, AsyncSnapshot<List<Height>> snapshot) {
          if (snapshot.data == null) {
            return Text("Incoming data...");
          } else if (snapshot.data.isEmpty) {
            return Text("No data");
          } else {
            return _buildBar(context, "height", snapshot.data[0]);
          }
        });
  }

  Widget _buildBar(BuildContext context, String text, Height height) {
    if (isFirstRender) {
      _height = height.value;
      isFirstRender = false;
    }
    return Container(
      child: Container(
        margin: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
            border: Border.all(width: 5.0, color: Colors.blueGrey),
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.transparent),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('HEIGHT: ${_height}cm', style: TextStyle(fontSize: 20.0)),
              ],
            ),
            Slider(
                value: _height.toDouble(),
                min: minHeight,
                max: maxHeight,
                activeColor: Colors.blue,
                inactiveColor: Colors.black,
                onChangeEnd: (double newValue) {
                  final newHeight =
                      Height(id: height.id, value: newValue.toInt());
                  widget.heightBloc.updateHeight(newHeight);
                },
                onChanged: (double newValue) {
                  setState(() {
                    _height = newValue.toInt();
                  });
                },
                semanticFormatterCallback: (double newValue) {
                  return '$newValue.round()';
                })
          ],
        ),
      ),
    );
  }
}
