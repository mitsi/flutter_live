import 'package:flutter/material.dart';
import 'package:flutter_demo_health_meetup/model/Weight.dart';
import 'package:flutter_demo_health_meetup/widgets/Home.dart';
import 'package:flutter_demo_health_meetup/widgets/home/Age.dart';
import 'package:flutter_demo_health_meetup/widgets/home/Gender.dart';
import 'package:flutter_demo_health_meetup/widgets/home/HeightWidget.dart';
import 'package:flutter_demo_health_meetup/widgets/home/WeightChart.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../mock/block.dart';

void main() {
  testWidgets(
      'Basic Displaymessage on empty listview and floating button, modal hidden by default',
      (WidgetTester tester) async {
    final mockBlocWeight = MockBlocWeight();
    final mockBlocHeight = MockBlocHeight();

    when(mockBlocWeight.weights)
        .thenAnswer((_) => Stream<List<Weight>>.value([]));

    await tester.pumpWidget(MaterialApp(
      home: Home(
        weightBloc: mockBlocWeight,
        heightBloc: mockBlocHeight,
      ),
    ));
    await tester.pump();

    expect(find.byType(Gender), findsOneWidget);
    expect(find.byType(Age), findsOneWidget);
    expect(find.byType(HeightWidget), findsOneWidget);
    expect(find.byType(WeightChart), findsOneWidget);
  });
}
