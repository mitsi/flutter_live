import 'dart:async';

import 'package:flutter_demo_health_meetup/database/Database.dart';
import 'package:flutter_demo_health_meetup/model/Height.dart';

class HeightDao {
  final DatabaseProvider dbProvider;
  HeightDao(this.dbProvider);

  Future<int> updateHeight(Height height) async {
    final db = await dbProvider.database;
    var result = db.update(HEIGHT_DATABASE, height.toDatabaseJson(), where: 'id = ?', whereArgs: [height.id]);
    return result;
  }

  Future<List<Height>> getHeights() async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result = await db.query(HEIGHT_DATABASE);

    List<Height> heights = result.isNotEmpty
        ? result.map((item) => Height.fromDatabaseJson(item)).toList()
        : [];
    return heights;
  }
}