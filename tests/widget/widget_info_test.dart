import 'package:flutter/material.dart';
import 'package:flutter_demo_health_meetup/widgets/Info.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Basic Display text and image', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Info(),
    ));
    var textWidget = find.byType(Text);
    var imageWidget = find.byType(Image);
    // Title + desc
    expect(textWidget, findsNWidgets(2));
    expect(imageWidget, findsOneWidget);
  });

  testWidgets('In a column', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Info(),
    ));
    // In column
   expect( find.descendant(
     of: find.byType(Column),
     matching: find.byType(Text),
   ), findsOneWidget);

   expect( find.descendant(
     of: find.byType(Column),
     matching: find.byType(Image),
   ), findsOneWidget);

  });
}
