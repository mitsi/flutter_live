import 'package:flutter/material.dart';
import 'package:flutter_demo_health_meetup/widgets/home/Age.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Display base widget', (WidgetTester tester) async {
    await tester.pumpWidget(Directionality(
      child: Age(),
      textDirection: TextDirection.ltr,
    ));
    var labelWidget = find.byKey(Key('label'));
    var textWidget = find.byKey(Key('selectedAge'));
    expect(textWidget, findsOneWidget);
    expect(getTextWidgetData(labelWidget), 'Votre age ?');
    expect(getTextWidgetData(textWidget), '20');
    expect(find.byKey(Key('plus')), findsOneWidget);
    expect(find.byKey(Key('minus')), findsOneWidget);
  });

  testWidgets('Can increase age by one', (WidgetTester tester) async {
    await tester.pumpWidget(Directionality(
      child: Age(),
      textDirection: TextDirection.ltr,
    ));
    await tester.tap(find.byKey(Key('plus')));
    await tester.pump();

    var textWidget = find.byKey(Key('selectedAge'));
    expect(getTextWidgetData(textWidget), '21');
  });

  testWidgets('Can decrease age by one', (WidgetTester tester) async {
    await tester.pumpWidget(Directionality(
      child: Age(),
      textDirection: TextDirection.ltr,
    ));
    await tester.tap(find.byKey(Key('minus')));
    await tester.pump();

    var textWidget = find.byKey(Key('selectedAge'));
    expect(getTextWidgetData(textWidget), '19');
  });
}

String getTextWidgetData(Finder textWidget) =>
    (textWidget.evaluate().single.widget as Text).data;
