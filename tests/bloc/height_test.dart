import 'dart:async';

import 'package:flutter_demo_health_meetup/bloc/HeightBloc.dart';
import 'package:flutter_demo_health_meetup/model/Height.dart';
import 'package:flutter_demo_health_meetup/repository/HeightRepository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

void main() {
  test('Can get added value by stream', () async {
    var mockHeightRepository = MockHeightRepository();
    var addedHeight = Height(id: 1, value: 180);
    bool firstCall = true;
    var emptyValues = List<Height>();
    when(mockHeightRepository.getAllHeights()).thenAnswer((_) {
      if (firstCall) {
        firstCall = false;
        return Future.value(emptyValues);
      }
      return Future.value([addedHeight]);
    });
    var heightBloc = HeightBloc(
        mockHeightRepository); //HeightRepository(HeightDao(DatabaseProvider.dbProvider))
    Stream<List<Height>> stream = heightBloc.heights;

    heightBloc.updateHeight(addedHeight);

    expectLater(stream, emitsInOrder([emptyValues, [addedHeight]]));
    verify(mockHeightRepository.updateHeight(captureAny)).called(1);
    verify(mockHeightRepository.getAllHeights()).called(2);
  });

  test('Can get value when subscribing', () async {
    var mockHeightRepository = MockHeightRepository();
    var addedHeight = Height(id: 1, value: 180);
    when(mockHeightRepository.getAllHeights())
        .thenAnswer((_) => Future.value([addedHeight]));
    var heightBloc = HeightBloc(
        mockHeightRepository); //HeightRepository(HeightDao(DatabaseProvider.dbProvider))
    Stream<List<Height>> stream = heightBloc.heights;
    List<Height> expected = [addedHeight];

    await expectLater(stream, emits(expected));
    verify(mockHeightRepository.getAllHeights()).called(1);
  });
}

class MockHeightRepository extends Mock implements HeightRepository {}
