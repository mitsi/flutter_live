import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:flutter_demo_health_meetup/bloc/HeightBloc.dart';
import 'package:flutter_demo_health_meetup/bloc/WeightBloc.dart';
import 'package:flutter_demo_health_meetup/model/Height.dart';
import 'package:flutter_demo_health_meetup/model/Weight.dart';
import 'package:intl/intl.dart';

class WeightChart extends StatefulWidget {
  WeightChart({Key key, this.weightBloc, this.heightBloc}) : super(key: key);
  final WeightBloc weightBloc;
  final HeightBloc heightBloc;

  @override
  _WeightChartState createState() => _WeightChartState();
}

class _WeightChartState extends State<WeightChart> {
  String _selected = "";
  Height _height;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: widget.heightBloc.heights,
      builder: (BuildContext context, AsyncSnapshot<List<Height>> heights) {
        return StreamBuilder(
          stream: widget.weightBloc.weights,
          builder: (BuildContext context, AsyncSnapshot<List<Weight>> weight) {
            if (weight.data == null) {
              return Text("History is null o.o");
            }
            if (weight.data.isEmpty) {
              return Text("No history entry...");
            }
            if (heights.data == null) {
              return Text("Height is null o.o");
            }
            if (heights.data.isEmpty) {
              return Text("No height entry...");
            }
            return wrapInHeightResponsive(
                buildDecoratedChart(context, weight, heights.data[0]));
          },
        );
      },
    );
  }

  buildChart(BuildContext context, AsyncSnapshot<List<Weight>> weights) {
    List<charts.Series<Weight, DateTime>> seriesList = [
      charts.Series<Weight, DateTime>(
        id: 'Weight evolution',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (Weight w, _) => w.date,
        measureFn: (Weight w, _) => w.value,
        data: weights.data,
      )
    ];
    final primaryAxisFormatter =
        charts.BasicNumericTickFormatterSpec((num value) => '$value kg');

    return Expanded(
        child: Container(
      child: charts.TimeSeriesChart(
        seriesList,
        animate: false,
        primaryMeasureAxis: new charts.NumericAxisSpec(
            tickProviderSpec:
                charts.BasicNumericTickProviderSpec(zeroBound: false),
            tickFormatterSpec: primaryAxisFormatter),
        selectionModels: [
          charts.SelectionModelConfig(
            type: charts.SelectionModelType.info,
            updatedListener: onTapOnChart,
          )
        ],
        behaviors: [
          new charts.LinePointHighlighter(
              showHorizontalFollowLine:
                  charts.LinePointHighlighterFollowLineType.none,
              showVerticalFollowLine:
                  charts.LinePointHighlighterFollowLineType.nearest),
          new charts.SelectNearest(
              eventTrigger: charts.SelectionTrigger.tapAndDrag),
        ],
        defaultRenderer: charts.LineRendererConfig(includePoints: true),
      ),
    ));
  }

  void onTapOnChart(charts.SelectionModel<DateTime> model) {
    final selectedDatum = model.selectedDatum;
    if (selectedDatum.isNotEmpty) {
      setState(() {
        _selected =
            "${DateFormat.yMMMd().format(selectedDatum[0].datum.date)} ${selectedDatum[0].datum.value}kg, BMI:${double.parse(((selectedDatum[0].datum.value / (_height.value * _height.value)) * 10000).toStringAsFixed(2))}";
      });
    }
  }

  Widget wrapInHeightResponsive(Widget child) {
    return Expanded(
      child: Container(
        padding: EdgeInsets.all(5),
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            border: Border.all(width: 5.0, color: Colors.blueGrey),
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.transparent),
        child: child,
      ),
    );
  }

  Widget buildDecoratedChart(BuildContext context,
      AsyncSnapshot<List<Weight>> weights, Height height) {
    _height = height;
    return Column(
      children: <Widget>[
        Text("Weight evolution"),
        buildChart(context, weights),
        Text("${_selected.isNotEmpty ? "Selected: $_selected" : "No selection"}"),
      ],
    );
  }
}
