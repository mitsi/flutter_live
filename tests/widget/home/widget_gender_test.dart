import 'package:flutter/material.dart';
import 'package:flutter_demo_health_meetup/widgets/home/Gender.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('In a column, a image then a text for male', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(body: Gender()),
    ));
    // In column
   expect( find.descendant(
     of: find.byType(Column),
     matching: find.byType(Image),
   ), findsNWidgets(2));

   expect( find.descendant(
     of: find.byType(Column),
     matching: find.byType(Text),
   ),  findsNWidgets(2));
  });

  testWidgets('Can retreive image and name for genders', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(body: Gender()),
    ));
    // In column
    assertForGender("resources/assets/female.png", "female", 0);
    assertForGender("resources/assets/male.png", "male", 1);
  });

  testWidgets('BorderLine color by default is blue grey then yellow when tapped', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(body: Gender()),
    ));
    // In column
    var predicate = getPredicateForContainer();
    var border = findColoredBorder(predicate);
    expect(border.top.color, equals(Colors.blueGrey));
    expect(border.isUniform, equals(true));
    await tester.tap((getPredicateForContainer()).first);
    await tester.pump();
    border = findColoredBorder(predicate);
    expect(border.top.color, equals(Colors.yellow));
    expect(border.isUniform, equals(true));
  });
}

Finder getPredicateForContainer() => find.byWidgetPredicate((Widget widget) => widget is Container && widget.decoration != null);

void assertForGender(String expectedPath, String name, int index) {
  var femaleImageWidget = find.byType(Image).evaluate().toList()[index].widget as Image;
  var assetImage = femaleImageWidget.image as AssetImage;
  expect(assetImage.assetName, equals(expectedPath));

  var femaleTextWidget = find.byType(Text).evaluate().toList()[index].widget as Text;
  expect(femaleTextWidget.data, equals(name));
}

BoxBorder findColoredBorder(Finder predicate) => ((predicate.evaluate().first.widget as Container).decoration as BoxDecoration).border;
