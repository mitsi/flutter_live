import 'dart:async';

import 'package:flutter_demo_health_meetup/model/Weight.dart';

class WeightBloc {
  final _weightRepository;
  final _weightController = StreamController<List<Weight>>.broadcast();

  get weights => _weightController.stream;

  WeightBloc(this._weightRepository) {
    getWeights();
  }

  getWeights() async {
    _weightController.sink.add(await _weightRepository.getAllWeights());
  }

  addWeight(Weight weight) async {
    await _weightRepository.addWeight(weight);
    getWeights();
  }

  deleteWeightById(int id) async {
    _weightRepository.deleteWeightById(id);
    getWeights();
  }

  dispose() {
    _weightController.close();
  }
}
