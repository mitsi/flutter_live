import 'dart:async';

import 'package:flutter_demo_health_meetup/model/Height.dart';
import 'package:flutter_demo_health_meetup/repository/HeightRepository.dart';

class HeightBloc {
  final HeightRepository _heightRepository;

  final _heightController = StreamController<List<Height>>.broadcast();

  get heights => _heightController.stream;

  HeightBloc(this._heightRepository) {
    getHeights();
  }

  getHeights() async {
    var event = await _heightRepository.getAllHeights();
    _heightController.sink.add(event);
  }

  updateHeight(Height height) async {
    _heightRepository.updateHeight(height);
    getHeights();
  }

  dispose() {
    _heightController.close();
  }
}
