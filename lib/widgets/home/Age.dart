import 'package:flutter/material.dart';

class Age extends StatefulWidget {
  @override
  _AgeState createState() => _AgeState();
}

class _AgeState extends State<Age> {
  int age = 20;

  @override
  Widget build(BuildContext context) => Container(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text("Votre age ?", key: Key("label")),
          Text("$age", key: Key("selectedAge")),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _buildButton("+", "plus", _increase),
              _buildButton("-", "minus", _decrease),
            ],
          )
        ],
      ));

  Widget _buildButton(String text, String key, Function onPress) => ButtonTheme(
      minWidth: 0,
      child: RaisedButton(
        shape: CircleBorder(),
        elevation: 5.0,
        color: Colors.blue,
        key: Key(key),
        child:
            Text(text, style: TextStyle(fontSize: 20.0, color: Colors.white)),
        onPressed: onPress,
      ));

  void _increase() {
    setState(() {
      age++;
    });
  }

  void _decrease() {
    setState(() {
      age--;
    });
  }
}
